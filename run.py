import os
from notes import note

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    note.run(host='127.0.0.1', port=port, debug=True)
